//
//  haSHArAppDelegate.h
//  https://github.com/jtphotog/haSHAr
//  Licensed under the terms of the MIT License, as specified below.
//

/*
 Copyright (c) 2011 Jeremy Torres, https://github.com/jtphotog/haSHAr
 
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:
 
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */

#import <Cocoa/Cocoa.h>
#import "haSHAr.h"

#define GENERATE_BUTTON_TAG_VAL 1
#define VERIFY_BUTTON_TAG_VAL   0

@interface haSHArAppDelegate : NSObject <NSApplicationDelegate,
                                         NSOpenSavePanelDelegate,
                                         haSHArDelegate>
{
@private
    NSWindow *__weak window;
    NSMatrix *__weak modeRadioButton;
    NSProgressIndicator *__weak progressInd;
    NSButton *__weak cancelButton;
    NSPathControl *__weak pathControl;
    NSButton *__weak selectDirectoryButton;
    NSButton *__weak startButton;
    haSHAr *hashar;
    NSURL *directoryToProcess;
    NSArray *fileURLs;
    NSArray *fileExts;
    NSUInteger totalFilesToProcess;
    NSUInteger processedFileCnt;
    NSOperationQueue * processQueue;
    NSBox *__weak outputBox;
    NSTextFieldCell *__weak fileProcessedLabel;
    NSTableView *__weak failedDigestsTableView;
}

@property (weak) IBOutlet NSTableView *failedDigestsTableView;
@property (weak) IBOutlet NSTextFieldCell *fileProcessedLabel;
@property (weak) IBOutlet NSBox *outputBox;
@property (weak) IBOutlet NSButton *selectDirectoryButton;
@property (weak) IBOutlet NSButton *startButton;
@property (weak) IBOutlet NSWindow *window;
@property (weak) IBOutlet NSMatrix *modeRadioButton;
@property (weak) IBOutlet NSProgressIndicator *progressInd;
@property (weak) IBOutlet NSButton *cancelButton;
@property (weak) IBOutlet NSPathControl *pathControl;

- (IBAction)selectDirectory:(id)sender;
- (IBAction)findSelectedButton:(id)sender;
- (IBAction)startProcessing:(id)sender;
- (IBAction)cancelProcessing:(id)sender;

// NSOpenSavePanelDelegate
- (void)panel:(id)sender didChangeToDirectoryURL:(NSURL *)url;
- (BOOL)panel:(id)sender validateURL:(NSURL *)url error:(NSError **)outError;

// haSHAr delegte
-(void)processedFile:(NSURL *)url;

-(void)fileProcessingComplete;


@end

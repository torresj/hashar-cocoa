//
//  AsyncFileIoHasher.m
//  https://bitbucket.org/torresj/hashar-cocoa
//  Licensed under the terms of the MIT License, as specified below.
//

/*
   Copyright (c) 2011 Jeremy Torres, https://bitbucket.org/torresj/hashar-cocoa

   Permission is hereby granted, free of charge, to any person obtaining
   a copy of this software and associated documentation files (the
   "Software"), to deal in the Software without restriction, including
   without limitation the rights to use, copy, modify, merge, publish,
   distribute, sublicense, and/or sell copies of the Software, and to
   permit persons to whom the Software is furnished to do so, subject to
   the following conditions:

   The above copyright notice and this permission notice shall be
   included in all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
   EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
   LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
   OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
   WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#import "AsyncFileIoHasher.h"

@implementation AsyncFileIoHasher

@synthesize messageDigest=_messageDigest;
@synthesize bytesRead=_bytesRead;

- (id)initWithAlgorithms:(HashAlgorithms *)algorithms {
    // ensure algorithms is valid
    if (algorithms == nil || [algorithms.algorithms count] < 1) {
        [NSException raise:@"InvalidHashAlgorithms"
                    format:@"Algorithm dictionary supplied is either null or empty"];
    }
    
    self = [super init];
    
	if (self) {
		// Initialization code here.
        _algorithms = algorithms;
		_messageDigest = [NSMutableData data];
		_bytesRead = 0;
		_sem = dispatch_semaphore_create(0);
	}
    
	return self;
}

- (id) init {
    return [self initWithAlgorithms:nil];
}

- (void)process:(NSURL *)url algorithmKey:(NSString *)algorithmKey {
    // create an instance of the algorithm to process
    HashAlgorithm * algorithm = [_algorithms algorithmFromKey:algorithmKey];
    [algorithm initializeAlgo];
     
    // Obtain the default high priority queue
	dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);

    // create path to specified URL
	_fileReaderChannel =
        dispatch_io_create_with_path(
            DISPATCH_IO_STREAM,
            [[url path] UTF8String],
             O_RDONLY,
             0,
             queue,
             ^(int error) {
                 if (error) {
                     printf("Error occured in FileReader Dispatch IO: %d\n", error);
                 }

                 // Cleanup code for normal channel operation. Assumes that
                 // dispatch_io_close was called elsewhere.
                 if (error == 0) {
                     _fileReaderChannel = NULL;
                 }
             });

    // Read in the file data, if valid file path
	if (_fileReaderChannel != NULL) {
		dispatch_io_read(
             _fileReaderChannel,
             0,
             SIZE_MAX,
             queue,
             ^(bool done, dispatch_data_t data, int error) {
                 if (data) {
                     // handle data received async
                     dispatch_data_apply(data, (dispatch_data_applier_t)
                                         ^(dispatch_data_t region, size_t offset, const void *buffer, size_t size) {
                                             // apply algorithm on data received
                                             [algorithm updateAlgo:buffer size:(CC_LONG)size];
//                                             CC_SHA1_Update(&ctx, buffer, (CC_LONG)size);
                                             _bytesRead += size;
                                             return true;
                                         });

                     if (done) {
                         // Read complete; finalize algorithm and cleanup IO
                         [algorithm finalizeAlgo];
                         //CC_SHA1_Final(md, &ctx);
                         
                         // set message digest
                         [_messageDigest appendBytes:(const void *)[algorithm messageDigest]
                                              length:[algorithm digestLength]];
                     
                         dispatch_io_close(_fileReaderChannel, 0);
                     
                         // signal semaphore indicating processing of file is complete
                         dispatch_semaphore_signal(_sem);
                      }

                      if (error) {
                         printf("Error occured in dispatch IO Read: %d\n", error);

                         dispatch_io_close(_fileReaderChannel, DISPATCH_IO_STOP);

                         dispatch_semaphore_signal(_sem);
                      }
                 }
             });

		dispatch_semaphore_wait(_sem, DISPATCH_TIME_FOREVER);
	}
}

@end

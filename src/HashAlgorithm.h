//
//  FileUtils.h
//  https://bitbucket.org/torresj/hashar-cocoa
//  Licensed under the terms of the MIT License, as specified below.
//

/*
   Copyright (c) 2011 Jeremy Torres, https://bitbucket.org/torresj/hashar-cocoa

   Permission is hereby granted, free of charge, to any person obtaining
   a copy of this software and associated documentation files (the
   "Software"), to deal in the Software without restriction, including
   without limitation the rights to use, copy, modify, merge, publish,
   distribute, sublicense, and/or sell copies of the Software, and to
   permit persons to whom the Software is furnished to do so, subject to
   the following conditions:

   The above copyright notice and this permission notice shall be
   included in all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
   EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
   LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
   OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
   WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */


#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonCrypto.h>
#import "FailedDigest.h"

@protocol HashChunkable <NSObject>
- (void)initializeAlgo;

- (void)updateAlgo:(const void *)data size:(CC_LONG)size;

- (void)finalizeAlgo;

@end

@interface HashAlgorithm : NSObject<HashChunkable>  {
@protected
    NSString * _name;
    NSString * _description;
    NSString * _sideCarFileExtension;
    unsigned int _digestLength;
    uint8_t * _messageDigest;
}

@property (readonly) NSString * name;
@property (readonly) NSString * sideCarFileExtension;
@property (readonly) unsigned int digestLength;
@property (readonly) uint8_t * messageDigest;

- (NSString *)description;

- (BOOL)performDigestVerification:(NSURL *)fileURL
                     fileDataHash:(NSData *)fileDataHash
                      sideCarHash:(NSData *)sideCarHash;
@end

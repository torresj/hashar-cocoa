//
//  YamlUtils.m
//  haSHArGUI
//
//  Created by Jeremy Torres on 9/23/12.
//
//

#import "YamlUtils.h"
#import "FailedDigest.h"
#import "FileUtils.h"
#include "yaml-cpp/yaml.h"
#include <string>

using std::string;

@implementation YamlUtils
/*
-(void)testEmitter {
    
    std::vector <int> squares;
    squares.push_back(1);
    squares.push_back(4);
    squares.push_back(9);
    squares.push_back(16);
    
    std::map <std::string, int> ages;
    ages["Daniel"] = 26;
    ages["Jesse"] = 24;
    
    YAML::Emitter out;
    out << YAML::BeginSeq;
    out << YAML::Flow << squares;
    out << ages;
    out << YAML::EndSeq;
    
    std::cout << "Here's the output YAML:\n" << out.c_str() << std::endl;
}
*/

+(NSString *)failedDigestsToYaml:(NSArray *)failedDigests {
    
    YAML::Emitter out;
    
    out.SetIndent(2);
    
    out << YAML::BeginSeq;
    
    NSString * yamlString;
    
    for (FailedDigest * fd in failedDigests)
    {
        out << YAML::BeginMap;
        
        out << YAML::Key << "File URL";
        out << YAML::Value << [[[fd fileURL] path] cStringUsingEncoding:NSUTF8StringEncoding];
        
        NSString * currentHash = [FileUtils digestBytesToString:[fd currentHash]];
        NSString * previousHash = [FileUtils digestBytesToString:[fd previousHash]];
        
        out << YAML::Key << "Previous Hash";
        out << YAML::Value << [previousHash cStringUsingEncoding:NSASCIIStringEncoding];
        
        out << YAML::Key << "Current Hash";
        out << YAML::Value << [currentHash cStringUsingEncoding:NSASCIIStringEncoding];
        
        out << YAML::EndMap;
    }
    
    out << YAML::EndSeq;
    
    yamlString = [NSString stringWithCString:out.c_str() encoding:NSASCIIStringEncoding];
    
    return yamlString;
}

@end

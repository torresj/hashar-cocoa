//
//  haSHAr.m
//  https://bitbucket.org/torresj/hashar-cocoa
//  Licensed under the terms of the MIT License, as specified below.
//

/*
 Copyright (c) 2011 Jeremy Torres, https://bitbucket.org/torresj/hashar-cocoa
 
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:
 
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */

#import "haSHAr.h"
#import "FileUtils.h"
#import "FailedDigest.h"
#import "AsyncFileIoHasher.h"
#import "ShaAlgorithm.h"

#import "DDFileLogger.h"
#import "DDTTYLogger.h"
#import "DDLog.h"

#include <CommonCrypto/CommonDigest.h>

// Set logger level
static const int ddLogLevel = LOG_LEVEL_VERBOSE;

// based on progress meter suggestion by "fvu" of stackoverflow.com
// http://stackoverflow.com/questions/1637587/c-libcurl-console-progress-bar
inline static void do_progress_bar(double totalFileCnt, double curFileCnt) {
    // how wide you want the progress meter to be
    static const int totaldotz = 40;
    double fractiondownloaded = curFileCnt / totalFileCnt;
    // part of the progressmeter that's already "full"
    int dotz = round(fractiondownloaded * totaldotz);
    
    // create the "meter"
    int ii = 0;
    
    printf("%3.0f%% (%3.0f/%3.0f) [", fractiondownloaded * 100, curFileCnt, totalFileCnt);
    // part that's full already
    for (; ii < dotz; ii++) {
        printf("=");
    }
    
    // remaining part (spaces)
    for (; ii < totaldotz; ii++) {
        printf(" ");
    }
    
    if (curFileCnt == totalFileCnt) {
        // last ... ensure we move to the next line
        printf("]\n");
    }
    else {
        // and back to line begin - do not forget the fflush to avoid output
        // buffering problems!
        printf("]\r");
    }
    
    fflush(stdout);
}

// Class extension for "private" functionality.
@interface haSHAr()

+ (FailedDigest *)handleFailedDigest:(NSURL *)fileURL
                        fileDataHash:(NSData *)fileDataHash
                         sideCarHash:(NSData *)sideCarHash;

- (void)dumpAlgorithms;

@end

@implementation haSHAr

@synthesize delegate;
@synthesize printToStdout;

- (id)init {
    self = [super init];
    
    if (self) {
        // Initialization code here.

        // Logger init
        _fileLogger = [[DDFileLogger alloc] init];
        _fileLogger.rollingFrequency = 60 * 60 * 24; // 24 hour rolling
        _fileLogger.logFileManager.maximumNumberOfLogFiles = 7;
        
        [DDLog addLogger:_fileLogger];
        //[DDLog addLogger:[DDTTYLogger sharedInstance]];
        
        _ioQueue = [[NSOperationQueue alloc] init];
        [_ioQueue setName:@"IOQueue"];

        _computeQueue = [[NSOperationQueue alloc] init];
        [_computeQueue setName:@"ComputeQueue"];

        _printQueue = [[NSOperationQueue alloc] init];
        [_printQueue setName:@"PrintQueue"];
        [_printQueue setMaxConcurrentOperationCount:1]; // limit width
        
        // instantiante and setup the algorithms dictionary
        _algorithms = [HashAlgorithms new];
        
//#ifdef DEBUGPRINT
        for (NSString * key in [_algorithms.algorithms allKeys]) {
            ShaAlgorithm * sha = [_algorithms.algorithms objectForKey:key];
            // Debug output for currently supported algorithms
            DDLogVerbose(@"Algorithm: %@.  Description: %@", [sha name], [sha description]);
        }
//#endif
    }
    
    return self;
}

- (void)dumpAlgorithms {
    
}

+ (FailedDigest *)handleFailedDigest:(NSURL *)fileURL
                        fileDataHash:(NSData *)fileDataHash
                         sideCarHash:(NSData *)sideCarHash {
    
    FailedDigest * failedDigest = [[FailedDigest alloc] init];

    failedDigest.fileURL = fileURL;
    failedDigest.currentHash = fileDataHash;
    failedDigest.previousHash = sideCarHash;

    return failedDigest;
}

- (NSArray *)verifyMessageDigests:(NSArray *)URLs
                         numfiles:(unsigned int)numfiles
                        algorithm:(NSString *)algorithm {
    // how many files to process concurrently
    _filesSem = dispatch_semaphore_create(numfiles);
    
    NSUInteger totalFileCnt = [URLs count];
    
    __block int curFileCnt = 0;
    
    NSMutableArray * failedDigests = [[NSMutableArray alloc] init];
    
    [_ioQueue setMaxConcurrentOperationCount:numfiles]; // limit width

    for (NSURL * URL in URLs) {
        // set by computeOperation
        __strong __block NSData * messageDigest = nil;
        // set by readDigestSidecarFileOperation
        __strong __block NSData * digestFromSideCarFile = nil;

        // Create operations to do the work for this URL

        NSBlockOperation * readAndComputeOperation =
            [NSBlockOperation blockOperationWithBlock:^{
                AsyncFileIoHasher * hashar = [[AsyncFileIoHasher alloc]
                                              initWithAlgorithms:_algorithms];

                 // file will be processed asyncronously, but we block
                 // until hash has been computed!
                 [hashar process:URL algorithmKey:algorithm];
                
                 messageDigest = [hashar messageDigest];

                 // increment semaphore to continue processing
                 dispatch_semaphore_signal(_filesSem);
             }];

        NSBlockOperation * readDigestSideCarFileOperation =
            [NSBlockOperation blockOperationWithBlock:^{
                 digestFromSideCarFile =
                     [FileUtils readMessageDigestFromURL:URL algorithmKey:algorithm];
             }];

        NSBlockOperation * computeOperation =
            [NSBlockOperation blockOperationWithBlock:^{
                HashAlgorithm * algo = [_algorithms.algorithms objectForKey:algorithm];
                
                BOOL isValidDigest = [algo performDigestVerification:URL
                                                        fileDataHash:messageDigest
                                                         sideCarHash:digestFromSideCarFile];
                if (isValidDigest) {
                    FailedDigest * failedDigest =
                        [haSHAr handleFailedDigest:URL fileDataHash:messageDigest sideCarHash:digestFromSideCarFile];
                    [failedDigests addObject:failedDigest];
                }
             }];

        NSBlockOperation * printOperation =
            [NSBlockOperation blockOperationWithBlock:^{
                if (printToStdout) {
                    do_progress_bar(totalFileCnt, ++curFileCnt);
                }
                
                // notify the delegate the file has been processed
                if ([self->delegate respondsToSelector:@selector(processedFile:)]) {
                    [self->delegate processedFile:URL];
                }
#ifdef DEBUGPRINT                
                else {
                    // DEBUG only
                    printf("Delegate does not respond to selector: "
                           "delegate is nil: %d\n", delegate == nil);
                }
#endif                
             }];

//        [printOperation setQueuePriority:NSOperationQueuePriorityHigh];

        // Set up dependencies between operations
        
        [readDigestSideCarFileOperation addDependency:readAndComputeOperation];
        [computeOperation addDependency:readAndComputeOperation];
        [computeOperation addDependency:readDigestSideCarFileOperation];
        [printOperation addDependency:computeOperation];

        // Add operations to appropriate queues
        
        [_ioQueue addOperation:readAndComputeOperation];
        [_computeQueue addOperation:computeOperation];
        [_ioQueue addOperation:readDigestSideCarFileOperation];
        [_printQueue addOperation:printOperation];
    }

    [_printQueue waitUntilAllOperationsAreFinished];

    return failedDigests;
}
 
- (void)generateMessageDigests:(NSArray *)URLs
                      numfiles:(unsigned int)numfiles
                     algorithm:(NSString *)algorithm {
    // how many files to process concurrently
    _filesSem = dispatch_semaphore_create(numfiles);

    NSUInteger totalFileCnt = [URLs count];
    
    __block int curFileCnt = 0;
    
    [_ioQueue setMaxConcurrentOperationCount:numfiles]; // limit width
    
    for (NSURL * URL in URLs) {
        // check if we're cancelled
        if (_isCancelled) {
            return;
        }

        if (!dispatch_semaphore_wait(_filesSem, DISPATCH_TIME_FOREVER)) {

            // set by computeOperation
            __strong __block NSData * messageDigest = nil;

            // Create operations to do the work for this URL

            NSBlockOperation * readAndComputeOperation =
                [NSBlockOperation blockOperationWithBlock:^{
                    AsyncFileIoHasher * hashar = [[AsyncFileIoHasher alloc]
                                                  initWithAlgorithms:_algorithms];
                    
                     // file will be processed asyncronously, but we block
                     // until hash has been computed!
                     [hashar process:URL algorithmKey:algorithm];

                     messageDigest = [hashar messageDigest];
                    
                     //DDLogInfo(@"MessageDigest: %@", messageDigest);

                     // increment semaphore to continue processing
                     dispatch_semaphore_signal(_filesSem);
                 }];

            NSBlockOperation * writeOperation =
                [NSBlockOperation blockOperationWithBlock:^{
                     [FileUtils writeMessageDigestRelativeToURL:messageDigest
                                                        fileURL:URL
                                                   algorithmKey:algorithm];
                 }];

            // assume write operation has highest priority
            [writeOperation setQueuePriority:NSOperationQueuePriorityHigh];

            NSBlockOperation * printOperation =
                [NSBlockOperation blockOperationWithBlock:^{
                    if (printToStdout) {
                        do_progress_bar(totalFileCnt, ++curFileCnt);
                    }
                    
                    // notify the delegate the file has been processed
                    if ([self->delegate respondsToSelector:@selector(processedFile:)]) {
                        [self->delegate processedFile:URL];
                    }
#ifdef DEBUGPRINT                
                    else {
                        // DEBUG only
                        printf("Delegate does not respond to selector: "
                               "delegate is nil: %d\n", delegate == nil);
                    }
#endif            
                 }];

            // Set up dependencies between operations
            
            [writeOperation addDependency:readAndComputeOperation];
            [printOperation addDependency:writeOperation];

            // Add operations to appropriate queues
            
            [_computeQueue addOperation:readAndComputeOperation];
            [_ioQueue addOperation:writeOperation];
            [_printQueue addOperation:printOperation];
        }
    }

    [_printQueue waitUntilAllOperationsAreFinished];
}

// TODO provide max wait as param to cancel method
- (void)cancel {
    static int waitSecs = 1;
    
    int maxWaitIntervals = 5;
    
    _isCancelled = TRUE;
    
    // cancel the operations in queues
    [_ioQueue cancelAllOperations];
    [_computeQueue cancelAllOperations];
    [_printQueue cancelAllOperations];
    
    // ensure all queues are empty
    NSArray * queues = [NSArray arrayWithObjects:_ioQueue, _computeQueue, _printQueue, nil];
    
    while (maxWaitIntervals > 0) {
        
        int queuesWithOps = 0;
        
        for (NSOperationQueue * queue in queues) {
            if ([queue operationCount] > 0) {
                queuesWithOps++;
            }
        }
        
        if (queuesWithOps > 0) {
            // debug
            printf("%d queues have remaininng operations\n", queuesWithOps);
            
            sleep(waitSecs);
            
            maxWaitIntervals--;
        }
        else {
            break;
        }
    }
}

@end

//
//  YamlUtils.h
//  haSHArGUI
//
//  Created by Jeremy Torres on 9/23/12.
//
//

#import <Foundation/Foundation.h>

@interface YamlUtils : NSObject

//-(void)testEmitter;

+ (NSString *)failedDigestsToYaml:(NSArray *)failedDigests;
@end

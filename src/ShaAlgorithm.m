//
//  ShaAlgorithm.m
//  https://bitbucket.org/torresj/hashar-cocoa
//  Licensed under the terms of the MIT License, as specified below.
//

/*
   Copyright (c) 2011 Jeremy Torres, https://bitbucket.org/torresj/hashar-cocoa

   Permission is hereby granted, free of charge, to any person obtaining
   a copy of this software and associated documentation files (the
   "Software"), to deal in the Software without restriction, including
   without limitation the rights to use, copy, modify, merge, publish,
   distribute, sublicense, and/or sell copies of the Software, and to
   permit persons to whom the Software is furnished to do so, subject to
   the following conditions:

   The above copyright notice and this permission notice shall be
   included in all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
   EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
   LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
   OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
   WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */


#import "ShaAlgorithm.h"

@implementation Sha1Algorithm
- (id)init {
    self = [super init];
    
    if (self) {
        // Setup protected ivars used as read-only properties
        _name = @"SHA1";
        _description = @"The SHA1 algorithm based on Apple's Common Crypto Implementation";
        _sideCarFileExtension = @".sha1";
        _digestLength = CC_SHA1_DIGEST_LENGTH;
        _messageDigest = &_digest[0];
    }
    
    return self;
}

- (void)initializeAlgo {
    [super initializeAlgo];
}

- (void)updateAlgo:(const void *)data size:(CC_LONG)size {
    // apply algorithm on data buffer
    CC_SHA1_Update(&_ctx, data, size);
}

- (void)finalizeAlgo {
    // finalize algorithm
    CC_SHA1_Final(_digest, &_ctx);
}
@end

@implementation Sha224Algorithm
- (id)init {
    self = [super init];
    
    if (self) {
        // Setup protected ivars used as read-only properties
        _name = @"SHA224";
        _description = @"The SHA224 algorithm based on Apple's Common Crypto Implementation";
        _sideCarFileExtension = @".sha224";
        _digestLength = CC_SHA224_DIGEST_LENGTH;
        _messageDigest = &_digest[0];
    }
    
    return self;
}

- (void)initializeAlgo {
    [super initializeAlgo];
}

- (void)updateAlgo:(const void *)data size:(CC_LONG)size {
    // apply algorithm on data buffer
    CC_SHA224_Update(&_ctx, data, size);
}

- (void)finalizeAlgo {
    // finalize algorithm
    CC_SHA224_Final(_digest, &_ctx);
}
@end

@implementation Sha256Algorithm
- (id)init {
    self = [super init];
    
    if (self) {
        // Setup protected ivars used as read-only properties
        _name = @"SHA256";
        _description = @"The SHA256 algorithm based on Apple's Common Crypto Implementation";
        _sideCarFileExtension = @".sha256";
        _digestLength = CC_SHA256_DIGEST_LENGTH;
        _messageDigest = &_digest[0];
    }
    
    return self;
}

- (void)initializeAlgo {
    [super initializeAlgo];
}

- (void)updateAlgo:(const void *)data size:(CC_LONG)size {
    // apply algorithm on data buffer
    CC_SHA256_Update(&_ctx, data, size);
}

- (void)finalizeAlgo {
    // finalize algorithm
    CC_SHA256_Final(_digest, &_ctx);
}
@end

@implementation Sha384Algorithm
- (id)init {
    self = [super init];
    
    if (self) {
        // Setup protected ivars used as read-only properties
        _name = @"SHA384";
        _description = @"The SHA384 algorithm based on Apple's Common Crypto Implementation";
        _sideCarFileExtension = @".sha384";
        _digestLength = CC_SHA384_DIGEST_LENGTH;
        _messageDigest = &_digest[0];
    }
    
    return self;
}

- (void)initializeAlgo {
    [super initializeAlgo];
}

- (void)updateAlgo:(const void *)data size:(CC_LONG)size {
    // apply algorithm on data buffer
    CC_SHA384_Update(&_ctx, data, size);
}

- (void)finalizeAlgo {
    // finalize algorithm
    CC_SHA384_Final(_digest, &_ctx);
}
@end

@implementation Sha512Algorithm
- (id)init {
    self = [super init];
    
    if (self) {
        // Setup protected ivars used as read-only properties
        _name = @"SHA512";
        _description = @"The SHA512 algorithm based on Apple's Common Crypto Implementation";
        _sideCarFileExtension = @".sha384";
        _digestLength = CC_SHA512_DIGEST_LENGTH;
        _messageDigest = &_digest[0];
    }
    
    return self;
}

- (void)initializeAlgo {
    [super initializeAlgo];
}

- (void)updateAlgo:(const void *)data size:(CC_LONG)size {
    // apply algorithm on data buffer
    CC_SHA512_Update(&_ctx, data, size);
}

- (void)finalizeAlgo {
    // finalize algorithm
    CC_SHA512_Final(_digest, &_ctx);
}
@end

@implementation ShaAlgorithm

- (id)init {
    self = [super init];
    
    if (self) {
        // Setup protected ivars used as read-only properties
        _name = @"sha";
        _description = @"The SHA algorithm containing the specific SHA1 algorithms ";
        _sideCarFileExtension = nil;
        _digestLength = 0;
    }
    
    return self;
}

- (void)initializeAlgo {
    //memset(_messageDigest, 0x00, _digestLength);
}

- (void)updateAlgo:(const void *)data size:(CC_LONG)size {
}

- (void)finalizeAlgo {
}

@end

//
//  HashAlgorithm.m
//  https://bitbucket.org/torresj/hashar-cocoa
//  Licensed under the terms of the MIT License, as specified below.
//

/*
   Copyright (c) 2011 Jeremy Torres, https://bitbucket.org/torresj/hashar-cocoa

   Permission is hereby granted, free of charge, to any person obtaining
   a copy of this software and associated documentation files (the
   "Software"), to deal in the Software without restriction, including
   without limitation the rights to use, copy, modify, merge, publish,
   distribute, sublicense, and/or sell copies of the Software, and to
   permit persons to whom the Software is furnished to do so, subject to
   the following conditions:

   The above copyright notice and this permission notice shall be
   included in all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
   EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
   LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
   OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
   WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */


#import "HashAlgorithm.h"
#import "DDLog.h"

// Set logger level
static const int ddLogLevel = LOG_LEVEL_VERBOSE;

@implementation HashAlgorithm

@synthesize name=_name, sideCarFileExtension=_sideCarFileExtension,
            digestLength=_digestLength, messageDigest=_messageDigest;
- (id)init {
    self = [super init];
    
    if (self) {
    }
    
    return self;
}

- (NSString *)description {
    return _description;
}

// protocol implementation
- (void)initializeAlgo {
    
}

- (void)updateAlgo:(const void *)data size:(CC_LONG)size {
    
}

- (void)finalizeAlgo {
    
}

// TODO move to HashAlgorithm class
- (BOOL)performDigestVerification:(NSURL *)fileURL
                     fileDataHash:(NSData *)fileDataHash
                      sideCarHash:(NSData *)sideCarHash {
    BOOL isFailure = FALSE;
    
    if (fileDataHash != nil && sideCarHash != nil) {
        NSUInteger fileDataHashLen;
        NSUInteger sideCarHashLen;
        
        fileDataHashLen = [fileDataHash length];
        sideCarHashLen = [sideCarHash length];
        
        // Failure conditions:
        //    a. The message digest is NULL
        //    b. The message digest length is 0
        //    c. The message digest length > EVP_MAX_MD_SIZE
        if (fileDataHashLen <= _digestLength &&
            sideCarHashLen <= _digestLength) {
            
            if (![fileDataHash isEqualToData:sideCarHash]) {
                
#ifdef DEBUGPRINT
                printf("Digests are NOT EQUAL for %s\n", [[fileURL path] UTF8String]);
#endif
                isFailure = TRUE;
            }
#ifdef DEBUGPRINT
            else {
                printf("Digests are equal for %s\n", [[fileURL path] UTF8String]);
            }
#endif
        }
#ifdef DEBUGPRINT
        else {
            NSLog(@"Invalid hashes to compare!");
        }
#endif
    }
    else if (fileDataHash != nil && sideCarHash == nil) {
        DDLogCWarn(@"File %@ does not have a side car hash\n", fileURL);
    }
    
    return isFailure;
}

@end

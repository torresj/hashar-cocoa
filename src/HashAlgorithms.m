//
//  HashAlgorithms.m
//  https://bitbucket.org/torresj/hashar-cocoa
//  Licensed under the terms of the MIT License, as specified below.
//

/*
   Copyright (c) 2011 Jeremy Torres, https://bitbucket.org/torresj/hashar-cocoa

   Permission is hereby granted, free of charge, to any person obtaining
   a copy of this software and associated documentation files (the
   "Software"), to deal in the Software without restriction, including
   without limitation the rights to use, copy, modify, merge, publish,
   distribute, sublicense, and/or sell copies of the Software, and to
   permit persons to whom the Software is furnished to do so, subject to
   the following conditions:

   The above copyright notice and this permission notice shall be
   included in all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
   EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
   LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
   OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
   WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */


#import "HashAlgorithms.h"
#import "ShaAlgorithm.h"

@implementation HashAlgorithms

@synthesize algorithms=_algorithmsDict;

- (id)init {
    self = [super init];
    
    if (self) {
        
        // Only SHA family supported (for now)
        _algorithmsDict = [NSMutableDictionary new];
        
        ShaAlgorithm * sha = [Sha1Algorithm new];
        
        [_algorithmsDict setObject:sha forKey:[sha name]];
        
        sha = [Sha224Algorithm new];
        [_algorithmsDict setObject:sha forKey:[sha name]];
        
        sha = [Sha256Algorithm new];
        [_algorithmsDict setObject:sha forKey:[sha name]];
        
        sha = [Sha384Algorithm new];
        [_algorithmsDict setObject:sha forKey:[sha name]];
        
        sha = [Sha512Algorithm new];
        [_algorithmsDict setObject:sha forKey:[sha name]];
    }
    
    return self;
}

- (HashAlgorithm *)algorithmFromKey:(NSString *)algoKey {
    // validate key
    if ([_algorithmsDict objectForKey:algoKey] != nil) {
        return [[[_algorithmsDict objectForKey:algoKey] class] new];
    } else {
        return nil;
    }
}

- (HashAlgorithm *)findAlgorithmByExtension:(NSString *)ext {
    HashAlgorithm * algorithm = nil;
    
    for (NSString * key in [_algorithmsDict allKeys]) {
        HashAlgorithm * algo = [_algorithmsDict objectForKey:key];
        if ([ext isEqualToString:[algo sideCarFileExtension]]) {
            // Return a _new_ instance of the algorithm.  This is required
            // to allow for multiple algorithms to run in parallel.
            algorithm = [self algorithmFromKey:key];
            break;
        }
    }
    
    return algorithm;
}

@end
